#include <iostream>
#include <vector>

using namespace std;

void printMatrix(vector< vector <int> > A) {
    cout << endl;
    for(int i = 0; i < A.size(); i++) {
        for(int j = 0; j < A[i].size(); j++) {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

vector < vector <int> > createDiagonalMatrix(vector < vector <int> > A) {
    vector < vector < int > > answer;
    int i, j, k, l;
    for(i = 0, j = 0; j < A[0].size(); j++) {
        vector <int> row;
        for(k = i, l = j; k != A.size() && l != -1; k++, l--) {
            row.push_back(A[k][l]);
        }
        answer.push_back(row);
    }
    for(i = 1, j = A[0].size() - 1; i < A.size(); i++) {
        vector <int> row;
        for(k = i, l = j; k != A.size() && l != -1; k++, l--) {
            row.push_back(A[k][l]);
        }
        answer.push_back(row);
    }
    return answer;
}

int main() {
    vector < vector <int> > matrix = [ [1, 2, 3], [4, 5, 6], [7, 8, 9] ];
    vector < vector <int> > answer =  createDiagonalMatrix(matrix);
    return 0;
}