#include <iostream>
#include <vector>

using namespace std;

void printMatrix(vector< vector <int> > A) {
    cout << endl;
    for(int i = 0; i < A.size(); i++) {
        for(int j = 0; j < A[i].size(); j++) {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

vector<vector<int> > createPascalTriangle(int A) {
    vector< vector <int> > array;
    for(int i = 0; i < A; i++) {
        vector<int> row(i+1);
        row[0] = row[i] = 1;
        for(int j = 1; j < i; j++) {
            cout << "Debug: i = " << i << ", j = " << j << endl;
            cout << array[i-1][j-1] << endl;
            printMatrix(array);
            row[j] = array[i-1][j] + array[i-1][j-1];
        }
        array.push_back(row);
    }
    return array;
}

int main() {
    int n;
    cin >> n;
    vector < vector <int> > A = createPascalTriangle(n);
    printMatrix(A);
    return 0;
}

