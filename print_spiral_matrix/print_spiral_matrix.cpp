#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;
    int A[n][n];

    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            A[i][j] = 0;
        }
    }

    int i, j, left, right, up, down;
    i = j = left = right = up = down = 0;
    string direction = "right";
    int curr = 1;

    while(curr <= n*n) {
        A[i][j] = curr;
        curr++;
        if (direction == "right") {
            if(j == n - right - 1) {
                direction = "down";
                up++;
                i++;
            }
            else {
                j++;
            }   
        }
        else if(direction == "down") {
            if(i == n - down - 1) {
                direction = "left";
                right++;
                j--;     
            }
            else {
                i++;
            }
        }
        else if (direction == "left") {
            if (j == left) {
                direction = "up";
                down++;
                i--;
            }
            else {
                j--;
            }
        }
        else if (direction == "up")
        {
            if (i == up)
            {
                direction = "right";
                left++;
                j++;
            }
            else
            {
                i--;
            }
        }
    }
    return 0;
}