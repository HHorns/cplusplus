#include <iostream>
#include <vector>

using namespace std;

void printVector(vector <int> A) {
    for (int i = 0; i < A.size(); i++)
        cout << A[i] << " ";
    cout << endl;
}

vector <int> primeSum(int N) {
    vector <int> primes(N+1);
    
    for (int i = 2; i <= N; i++)
        primes[i] = 1;
    for(int i = 2; i*i <= N; i++) {
        if(primes[i]) {
            for(int j = i*i; j <= N; j += i)
                primes[j] = 0;
        }
    }

    vector <int> solution;

    if(N == 4) {
        solution.push_back(2);
        solution.push_back(2);
        return solution;
    }

    for(int i = 3; i <= N/2; i += 2) {
        if(primes[i] && primes[N-i]) {
            solution.push_back(i);
            solution.push_back(N-i);
            break;
        }
    }

    return solution;
}

int main() {
    int N;
    cin >> N;

    vector <int> solution = primeSum(N);
    printVector(solution);

    return 0;
}
