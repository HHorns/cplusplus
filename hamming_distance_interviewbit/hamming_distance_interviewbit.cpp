#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

int modulo = 1000000007;
map <int, int> setBitCount;

void printVector(vector<int> A)
{
    for (int i = 0; i < A.size(); i++)
        cout << A[i] << " ";
    cout << endl;
}

int findSetBitCount(int n) {
    if(n == 0)
        return 0;
    if(n < 1000000)
        return findSetBitCount(n & (n - 1)) + 1;
    if(setBitCount.find(n) == setBitCount.end())
        setBitCount[n] = findSetBitCount(n & (n - 1)) + 1;
    return setBitCount[n];
}

int hammingDistance(vector <int> &A) {
    long long answer = 0;
    map <int, int> B;
    for(int i = 0; i < A.size(); i++)
        B[A[i]]++;
    for (map<int, int>::iterator it = B.begin(); it != B.end(); it++) {
        for (map<int, int>::iterator j = next(it); j != B.end(); j++) {
            answer = (answer + (2 * it->second * j->second) * findSetBitCount(it->first ^ j->first)) % modulo;
        }
    }
    return (int)answer;
}

int hammingDistance2(vector<int> &A) {
    vector <long long> zeroes(64, 0);
    vector<long long> ones(64, 0);
    for(int i = 0; i < A.size(); i++) {
        int x = A[i];
        for(int j = 63; j >= 0; j--) {
            x%2 == 0 ? zeroes[j]++ : ones[j]++;
            x /= 2;
        }
    }
    // printVector(zeroes);
    // printVector(ones);
    long long answer = 0;
    for(int i = 0; i< 64; i++) {
        answer = (answer + zeroes[i] * ones[i]) % modulo;
    }
    return (answer*2) % modulo;
}

int main()
    {
        int N;
        cin >> N;
        vector<int> A;
        while (N--)
        {
            int x;
            cin >> x;
            A.push_back(x);
        }
        cout << hammingDistance(A) << endl;
        cout << hammingDistance2(A) << endl;
        return 0;
    }