#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

bool compareString(string s1, string s2) {
    return s1 + s2 < s2 + s1;
}

string largestNumber(vector < int > A) {
    vector <string> B;
    for(int i = 0; i < A.size(); i++)
        B.push_back(to_string(A[i]));
    sort(B.begin(), B.end(), compareString);
    string answer = "";
    for(int i = B.size() - 1; i != -1; i--)
        answer += B[i];
    while(answer[0] == '0' && answer.size() > 1)
    {
        
        answer.erase(0, 1);
    }
    return answer;
}

int main() {
    vector<int> A;
    int x = 5;
    while(x--) {
        int y;
        cin >> y;
        A.push_back(y);
    }
    string answer = largestNumber(A);
    cout << answer;
    return 0;
}